import React,{useEffect} from 'react'
import {LinkContainer} from 'react-router-bootstrap'
import {Table,Button,Row,Col} from 'react-bootstrap'
import {useDispatch,useSelector} from 'react-redux'
import Loader from '../components/Loader'
import Message from '../components/Message'
import {useNavigate,useParams} from 'react-router-dom'
import Swal from 'sweetalert2'
import {listProducts,deleteProduct,createProduct} from '../actions/productActions'
import { PRODUCT_CREATE_RESET,PRODUCT_DETAILS_RESET } from '../constants/productConstants'

const ProductListScreen = () => {
    const {id}=useParams()
    const dispatch=useDispatch()
    const navigate=useNavigate()

    const productList = useSelector(state=>state.productList)
    const{loading,error,products}=productList

    const userLogin = useSelector(state=>state.userLogin)
    const{userInfo}=userLogin

    const productDelete = useSelector(state=>state.productDelete)
    const{loading:loadingDelete,success:successDelete,error:errorDelete}=productDelete
  
    const productCreate = useSelector(state=>state.productCreate)
    const{loading:loadingCreate,success:successCreate,error:errorCreate,product:createdProduct}=productCreate


    useEffect(()=>{
        dispatch({type: PRODUCT_CREATE_RESET})
        dispatch({type: PRODUCT_DETAILS_RESET})
        if(!userInfo.isAdmin){
            navigate('/login')
        }
        
        if(successCreate){
            navigate(`/admin/product/${createdProduct._id}/edit`)
        }else{
            dispatch(listProducts())
        }

    },[dispatch,userInfo,navigate,successDelete,successCreate,createdProduct])

    const createProductHandler=(product)=>{
        dispatch(createProduct(id))
    }

     const deleteHandler=(id)=>{
        Swal.fire({
            title: 'Are you sure you want to delete product?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#22577E',
            cancelButtonColor: '#D77FA1',
            confirmButtonText: 'Yes'
          }).then((result) => {
            if (result.isConfirmed) {
              Swal.fire(
                'Product removed',
                'You have deleted product.'
              )
              dispatch(deleteProduct(id))
              
            }
          })
        console.log('delete')
    }

  return (
    <>
    <Row className='align-items-center'>
        <Col>
            <h1>Products</h1>
        </Col>
        <Col className='text-end'>
            <Button className='my-3' onClick={createProductHandler}>
            <i className="fa-solid fa-plus me-2"></i>Create Product
            </Button>
        </Col>
    </Row>
        {loadingDelete && <Loader/>}
        {errorDelete && <Message variant='danger'>{errorDelete}</Message>}
        {loadingCreate && <Loader/>}
        {errorCreate && <Message variant='danger'>{errorCreate}</Message>}
        {loading?<Loader/>:error?<Message variant='danger'>{error}</Message>
        : (
            <Table striped bordered hover responsive className='table-sm'>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>PRICE</th>
                        <th>CATEGORY</th>
                        <th>BRAND</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {products.map(product=>(
                        <tr key={product._id}>
                            <td>{product._id}</td>
                            <td>{product.name}</td>
                            <td>{product.price}</td>
                            <td>{product.category}</td>
                            <td>{product.brand}</td>                            
                            <td>
                                <LinkContainer to={`/admin/product/${product._id}/edit`}>
                                    <Button variant='light' className='btn-sm'>
                                    <i className="fa-solid fa-pen-to-square fa-lg"></i>
                                    </Button>
                                </LinkContainer>
                                <Button variant='danger' className='btn-sm' onClick={()=>
                                deleteHandler(product._id)}>
                                    <i className="fa-solid fa-trash fa-lg"></i>
                                </Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        )
        }
    </>
  )
}

export default ProductListScreen