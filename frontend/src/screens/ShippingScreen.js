import React,{useState} from 'react'
import {useNavigate} from 'react-router-dom'
import {Form,Button} from 'react-bootstrap'
import {useDispatch,useSelector} from 'react-redux'
import FormContainer from '../components/FormContainer'
import CheckoutSteps from '../components/CheckoutSteps'
import {saveShippingAddress} from '../actions/cartActions'

const ShippingScreen = () => {
    const cart = useSelector((state)=>state.cart)
    const {shippingAddress} = cart

    const [street,setStreet]=useState(shippingAddress.street)
    const [barangay,setBarangay]=useState(shippingAddress.barangay)
    const [city_mun,setCity_mun]=useState(shippingAddress.city_mun)
    const [province,setProvince]=useState(shippingAddress.province)

    const dispatch=useDispatch()
    const navigate=useNavigate()
       
    const submitHandler=(e)=>{
        e.preventDefault()
        dispatch(saveShippingAddress({street,barangay,city_mun,province}))
        navigate('/payment')
    }

  return <FormContainer>
  <CheckoutSteps step1 step2/>
    <h1>Shipping</h1>
    <Form onSubmit={submitHandler}>
        <Form.Group controlId='street' className='mb-3'>
            <Form.Label>House no./Street</Form.Label>
            <Form.Control
                type='text' 
                placeholder='Enter house no./street' 
                value={street}
                onChange={(e)=>setStreet(e.target.value)}>
            </Form.Control> 
          </Form.Group>

          <Form.Group controlId='barangay' className='mb-3'>
            <Form.Label>Barangay</Form.Label>
            <Form.Control
                type='text' 
                placeholder='Enter barangay' 
                value={barangay}
                onChange={(e)=>setBarangay(e.target.value)}>
            </Form.Control> 
          </Form.Group>

          <Form.Group controlId='city_mun' className='mb-3'>
            <Form.Label>City/Municipality</Form.Label>
            <Form.Control
                type='text' 
                placeholder='Enter city/municipality' 
                value={city_mun}
                onChange={(e)=>setCity_mun(e.target.value)}>
            </Form.Control> 
          </Form.Group>

          <Form.Group controlId='province' className='mb-3'>
            <Form.Label>Province</Form.Label>
            <Form.Control
                type='text' 
                placeholder='Enter province' 
                value={province}
                onChange={(e)=>setProvince(e.target.value)}>
            </Form.Control> 
          </Form.Group>

          <Button type='submit' variant='primary'>
              Continue
          </Button>

    </Form>
  </FormContainer>
}

export default ShippingScreen