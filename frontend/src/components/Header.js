import React from 'react'
import { Link,useNavigate } from 'react-router-dom'
import {useDispatch,useSelector} from 'react-redux'
import {Container,Nav,Navbar, NavDropdown} from 'react-bootstrap'
import {logout} from '../actions/userActions'
import Swal from 'sweetalert2'
import SearchBox from './SearchBox'

const Header = () => {
    const dispatch=useDispatch()
    const userLogin = useSelector(state=> state.userLogin)
    const {userInfo} = userLogin
    const navigate = useNavigate()

    const logoutHandler=()=>{
        
        Swal.fire({
            title: 'Are you sure you want to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#22577E',
            cancelButtonColor: '#D77FA1',
            confirmButtonText: 'Yes'
          }).then((result) => {
            if (result.isConfirmed) {
              Swal.fire({
                icon: 'success',
                title: 'Logout successful',
                showConfirmButton: false,
                timer: 1500
              })
              dispatch(logout())
              navigate('/')
            }
          })
    }

  return (
     <header>
        <Navbar bg="primary" variant="dark" expand="lg" collapseOnSelect>
            <Container>
                <Navbar.Brand as={ Link } to="/">
                <img
                    alt=""
                    src="/icon.png"
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                  />{' '}
                  Get-IT
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <SearchBox/>
                    <Nav className="ms-auto">
                        <Nav.Link as={ Link } to="/cart"><i className='fas fa-shopping-cart me-2'></i>
                        Cart</Nav.Link>
                        {userInfo ? (
                          
                            <NavDropdown title={userInfo.name} id='username'>
                                <NavDropdown.Item as={ Link } to="/profile">Profile</NavDropdown.Item>
                                <NavDropdown.Item onClick={logoutHandler}>Logout</NavDropdown.Item>
                            </NavDropdown>
                        ) : 
                        <Nav.Link as={ Link } to="/login"><i className='fas fa-user me-2'></i>
                        Sign In</Nav.Link>  
                        } 
                        {userInfo && userInfo.isAdmin && (
                          <NavDropdown title='Admin' id='adminMenu'>
                                <NavDropdown.Item as={ Link } to="/admin/userlist">Users</NavDropdown.Item>
                                <NavDropdown.Item as={ Link } to="/admin/productlist">Products</NavDropdown.Item>
                                <NavDropdown.Item as={ Link } to="/admin/orderlist">Orders</NavDropdown.Item>
                            </NavDropdown>
                        ) }                 
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
     </header> 
  )
}

export default Header