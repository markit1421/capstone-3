import express from 'express'
import { getProductDetails, 
    getProducts, 
    updateProduct,
    deleteProduct,
    createProduct,
    createProductReview,
    getTopProducts
 } from '../controllers/productController.js'
import { protect,admin } from '../middleware/authMiddleware.js'

const router =express.Router()

router.route('/').get(getProducts).post(protect,admin,createProduct)
router.route('/:id/reviews').post(protect,createProductReview)
router.get('/top',getTopProducts)
router.route('/getDetails/:id').get(getProductDetails)

router.route('/:id').get(getProductDetails).delete(protect,admin,deleteProduct).put(protect,admin,updateProduct)



export default router