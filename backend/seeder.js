import mongoose from "mongoose"
import dotenv from 'dotenv'
import users from './data/users.js'
import products from './data/products.js'
import User from './models/User.js'
import Product from './models/Product.js'
import Order from './models/Order.js'
import connectDB from "./config/db.js"

dotenv.config()

connectDB()

const importData = async () =>{
    try {
        await Order.deleteMany()
        await User.deleteMany()
        await Product.deleteMany()

        const createdUser = await User.insertMany(users)
        const adminUser = createdUser[0]._id

        const sampleProducts=products.map(products=>{
            return {...products, user:adminUser}
        })

        await Product.insertMany(sampleProducts)

        console.log('Data Imported!')

    } catch (error) {
        console.error(`${error}`)
        process.exit(1)
    }
}

const destroyData = async () =>{
    try {
        await Order.deleteMany()
        await User.deleteMany()
        await Product.deleteMany()

        console.log('Data Destroyed!')
        process.exit()
    } catch (error) {
        console.error(`${error}`)
        process.exit(1)
    }
}

if(process.argv[2] === '-d'){
    destroyData()
}else{
    importData()
}