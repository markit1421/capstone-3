import bcrypt from 'bcryptjs'

const users = [
    {
        name: 'Admin',
        email:'admin@example.com',
        password: bcrypt.hashSync('admin',10),
        isAdmin:true
    },
    {
        name: 'Juan Tamad',
        email:'juan@example.com',
        password: bcrypt.hashSync('123456',10)
    },
    {
        name: 'Jane Doe',
        email:'jane@example.com',
        password: bcrypt.hashSync('123456',10)
    }
]

export default users